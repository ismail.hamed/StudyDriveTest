# Studydrive Documentation

**API Documentation:**

    localhost:<port>/docs 

**Migrate:**

    docker-compose run --rm artisan migrate 

**Generate Seeders:**

    docker-compose run --rm artisan module:seed 

**Run Test:**

    docker-compose run --rm artisan test

**You can read command file to more information about docker**

**All students have password 'secret' when generate seeders**
