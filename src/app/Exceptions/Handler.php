<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($exception instanceof AuthenticationException) {
            return $this->errorMessage("Unauthorized", Response::HTTP_UNAUTHORIZED);
        }
        if ($exception instanceof AuthorizationException) {
            return $this->errorMessage("This action is unauthorized.", Response::HTTP_FORBIDDEN);
        }
        if ($exception instanceof ValidationException)
            return $this->successResponse([
                'message' => $exception->getMessage(),
                'errors' => $exception->validator->messages(),
                'status_code' => $exception->status,
            ], $exception->status);

        if ($exception instanceof ModelNotFoundException) {
            return $this->errorMessage("404 Not Found.", Response::HTTP_NOT_FOUND);
        }

//        if ($exception instanceof TokenExpiredException ||
//            $exception instanceof TokenInvalidException ||
//            $exception instanceof TokenBlacklistedException)

        return parent::render($request, $exception);

    }

    protected function errorMessage($message, $code)
    {
        return response()->json(['message' => $message, 'status_code' => $code], $code);
    }

    public function successResponse($data, $code): JsonResponse
    {
        return response()->json($data, $code);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('auth.login'));
    }

}
