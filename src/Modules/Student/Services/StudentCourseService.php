<?php

namespace Modules\Student\Services;

use Illuminate\Support\Facades\Auth;
use Modules\Core\Services\BaseService;
use Modules\Course\Contracts\CourseInterface;
use Modules\Course\Models\Course;
use Modules\Student\Contracts\StudentInterface;
use Symfony\Component\HttpFoundation\Response;

class StudentCourseService extends BaseService
{
    protected $student;
    protected $course;

    public function __construct(StudentInterface $student, CourseInterface $course)
    {
        $this->student = $student;
        $this->course = $course;
    }

    public function registerForCourse(Course $course)
    {
        if ($course->available()) {
            $student = Auth::user();
            $course->students()->attach($student->id);
            $message = trans('student::messages.register_for_course_successfully');
            return $this->successResponse(null, $message);
        } else {
            $message = trans('student::messages.course_is_full');
            return $this->errorResponse($message, Response::HTTP_CONFLICT);
        }
    }

}
