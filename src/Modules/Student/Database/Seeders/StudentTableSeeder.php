<?php

namespace Modules\Student\Database\Seeders;

use App\Models\WorkHour;
use Illuminate\Database\Seeder;
use Modules\Student\Models\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::factory()->count(25)->create();
    }
}
