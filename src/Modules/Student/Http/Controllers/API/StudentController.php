<?php

namespace Modules\Student\Http\Controllers\API;

use Illuminate\Http\Request;
use Modules\Auth\Http\Requests\ForgotPasswordRequest;
use Modules\Auth\Http\Requests\RefreshTokenRequest;
use Modules\Core\Http\Controllers\API\ApiBaseController;
use Modules\Course\Models\Course;
use Modules\Student\Services\StudentCourseService;

/**
 * @group Student management
 *  APIs for Student
 */
class StudentController extends ApiBaseController
{
    protected $studentCourseService;

    public function __construct(StudentCourseService $studentCourseService)
    {
        $this->studentCourseService = $studentCourseService;
    }

    /**
     * Register
     *
     * This endpoint lets student to register for specific course
     * @authenticated
     * @urlParam course integer required The ID of the course.
     * @responseFile Modules/Student/Storage/responses/register.json
     * @return mixed
     */
    public function registerForCourse(Request $request, Course $course)
    {
        $result = $this->studentCourseService->registerForCourse($course);
        return $this->apiResponse($result);
    }

}
