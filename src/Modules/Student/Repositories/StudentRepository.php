<?php

namespace Modules\Student\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\Student\Contracts\StudentInterface;
use Modules\Student\Models\Student;

class StudentRepository extends BaseRepository implements StudentInterface
{

    public function __construct(Student $model)
    {
        parent::__construct($model); // Inject the model that you need to build queries from
    }

}
