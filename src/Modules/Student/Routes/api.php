<?php

use Modules\Student\Http\Controllers\API\StudentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('student')->group(function () {
    Route::controller(StudentController::class)->group(function () {
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::post('register/{course}', 'registerForCourse');
        });
    });
});
