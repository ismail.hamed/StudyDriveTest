<?php

namespace Modules\Student\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Course\Models\Course;
use Modules\Student\Models\Student;
use Symfony\Component\HttpFoundation\Response;


class RegisterForCourseTest extends ApiTestCase
{
    public function test_register_for_course_if_user_not_registered()
    {
        $course = Course::factory()->create();
        $response = $this->postJson('student/register/' . $course->id);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => "Unauthorized", 'status_code' => Response::HTTP_UNAUTHORIZED]);
    }

    public function test_register_for_course_if_course_not_exist()
    {
        $student = Student::factory()->create();
        $response = $this->actingAs($student)->postJson('student/register/' . 1);
        $response->assertStatus(Response::HTTP_NOT_FOUND)
            ->assertJson(['message' => "404 Not Found.", 'status_code' => Response::HTTP_NOT_FOUND]);
    }

    public function test_register_for_course_if_course_is_full()
    {
        $student = Student::factory()->create();
        $course = Course::factory()
            ->has(Student::factory()->count(3))
            ->create(['capacity' => 3]);
        $response = $this->actingAs($student)->postJson('student/register/' . $course->id);
        $response->assertStatus(Response::HTTP_CONFLICT)
            ->assertJson(['message' => "Sorry,the course is full.", 'status_code' => Response::HTTP_CONFLICT]);
    }

    public function test_register_for_course()
    {
        $student = Student::factory()->create();
        $course = Course::factory()->create();
        $response = $this->actingAs($student)->postJson('student/register/' . $course->id);
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(['message' => "Successfully register for course.", 'status_code' => Response::HTTP_OK]);
        $this->assertDatabaseCount('registration', 1);
    }
    // Todo not allow to student to register for course more than one
}
