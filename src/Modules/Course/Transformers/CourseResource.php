<?php

namespace Modules\Course\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'capacity' => $this->capacity,
            'status' => $this->capacity ? 'Available' : 'Unavailable',
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}

