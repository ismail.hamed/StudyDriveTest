<?php

namespace Modules\Course\Database\Seeders;

use Illuminate\Database\Seeder;

class CourseDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CourseTableSeeder::class);
    }
}
