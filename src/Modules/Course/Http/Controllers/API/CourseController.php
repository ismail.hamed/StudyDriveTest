<?php

namespace Modules\Course\Http\Controllers\API;

use Modules\Core\Http\Controllers\API\ApiBaseController;
use Modules\Course\Services\CourseService;
use Modules\Course\Transformers\CourseResource;

/**
 * @group Course management
 *  APIs for Course
 */
class CourseController extends ApiBaseController
{
    protected $CourseService;

    public function __construct(CourseService $CourseService)
    {
        $this->CourseService = $CourseService;
    }

    /**
     * Courses
     *
     * This endpoint lets student to show courses
     * @authenticated
     * @queryParam page int Field to get specific page. Defaults to 1.
     * @queryParam per_page int Field to get specific count per page. Defaults to 25.
     * @responseFile Modules/Course/Storage/responses/index.json
     * @return mixed
     */
    public function index()
    {
        $result = $this->CourseService->getCourses();
        if ($result['result']) {
            $result['data'] = CourseResource::collection($result['data']);
        }
        return $this->apiResponse($result);
    }
}
