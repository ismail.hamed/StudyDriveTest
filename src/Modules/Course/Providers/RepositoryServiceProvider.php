<?php

namespace Modules\Course\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Course\Contracts\CourseInterface;
use Modules\Course\Models\Course;
use Modules\Course\Repositories\CachingCourseRepository;
use Modules\Course\Repositories\CourseRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CourseInterface::class, function ($app) {
            return new CachingCourseRepository(new CourseRepository(new Course));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
