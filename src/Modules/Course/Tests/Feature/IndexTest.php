<?php

namespace Modules\Course\Tests\Feature;

use Closure;
use Illuminate\Support\Facades\Cache;
use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Course\Models\Course;
use Modules\Student\Models\Student;
use Symfony\Component\HttpFoundation\Response;


class IndexTest extends ApiTestCase
{
    public function test_show_courses_if_user_not_registered()
    {
        Course::factory()->count(20)->create();
        $response = $this->getJson('courses');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => "Unauthorized", 'status_code' => Response::HTTP_UNAUTHORIZED]);
    }

    public function test_show_courses()
    {
        $student = Student::factory()->create();
        $courses = Course::factory()->count(20)->create();
        $courses = $courses->get(6);
        $response = $this->actingAs($student)->getJson('courses?page=2&per_page=5');
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonCount(5, 'data')
            ->assertJsonFragment($this->data_response($courses))
            ->assertJson($this->data_response_pagination());
    }

    public function test_cache_show_courses()
    {
        $key = 'courses-2-5';
        Cache::shouldReceive('remember')
            ->once()
            ->with($key, 10, Closure::class)
            ->andReturn(Course::paginate(5));
        $student = Student::factory()->create();
        Course::factory()->count(20)->create();
        $response = $this->actingAs($student)->getJson('courses?page=2&per_page=5');
        $response->assertOk();

    }

    private function data_response(Course $course)
    {
        return [
            'id' => $course->id,
            'capacity' => $course->capacity,
            'status' => $course->capacity ? 'Available' : 'Unavailable',
            'created_at' => $course->created_at,
            'updated_at' => $course->updated_at,
        ];
    }

    protected function data_response_pagination()
    {
        return
            [
                'links' =>
                    [
                        'first' => 'http://localhost/api/courses?page=1',
                        'last' => 'http://localhost/api/courses?page=4',
                        'prev' => 'http://localhost/api/courses?page=1',
                        'next' => 'http://localhost/api/courses?page=3',
                    ],
                'meta' =>
                    [
                        'current_page' => 2,
                        'from' => 6,
                        'last_page' => 4,
                        'path' => 'http://localhost/api/courses',
                        'per_page' => 5,
                        'to' => 10,
                        'total' => 20,

                    ]
            ];
    }

}
