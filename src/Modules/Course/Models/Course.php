<?php

namespace Modules\Course\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Course\Database\factories\CourseFactory;
use Modules\Student\Models\Student;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'capacity',
    ];

    protected static function newFactory()
    {
        return CourseFactory::new();
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'registration');
    }

    public function available()
    {
        return ($this->students->count() < $this->capacity) ? true : false;
    }
}
