<?php

namespace Modules\Course\Services;

use Modules\Core\Services\BaseService;
use Modules\Course\Contracts\CourseInterface;

class CourseService extends BaseService
{
    protected $course;

    public function __construct(CourseInterface $course)
    {
        $this->course = $course;
    }

    public function getCourses()
    {
        $courses = $this->course->all();
        return $this->successResponse($courses);
    }

}
