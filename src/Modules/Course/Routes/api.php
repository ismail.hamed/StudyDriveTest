<?php

use Modules\Course\Http\Controllers\API\CourseController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('courses')->group(function () {
    Route::middleware(['auth:sanctum'])->group(function () {
        Route::apiResource('/', CourseController::class)->only(['index']);
    });
});
