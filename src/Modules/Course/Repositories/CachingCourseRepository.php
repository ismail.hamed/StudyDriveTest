<?php

namespace Modules\Course\Repositories;

use Illuminate\Support\Facades\Cache;
use Modules\Core\Repositories\BaseRepository;
use Modules\Course\Contracts\CourseInterface;

class CachingCourseRepository extends BaseRepository implements CourseInterface
{

    protected $course;

    public function __construct(CourseInterface $course)
    {
        $this->course = $course;
    }

    public function all()
    {
        $page = request('page', 1);
        $per_page = request('per_page', 25);
        $key = 'courses-' . $page . '-' . $per_page;
        return Cache::remember($key, $minutes = 10, function () {
            return $this->course->all();
        });
    }
}
