<?php

namespace Modules\Course\Repositories;

use Modules\Core\Repositories\BaseRepository;
use Modules\Course\Contracts\CourseInterface;
use Modules\Course\Models\Course;

class CourseRepository extends BaseRepository implements CourseInterface
{

    public function __construct(Course $model)
    {
        parent::__construct($model); // Inject the model that you need to build queries from
    }

}
