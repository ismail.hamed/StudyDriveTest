<?php

namespace Modules\Core\Services;

use Modules\Core\Traits\ServiceResponse;

class BaseService
{
    use ServiceResponse;
}
