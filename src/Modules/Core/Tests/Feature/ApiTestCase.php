<?php

namespace Modules\Core\Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\Core\Tests\TestCase;

abstract class ApiTestCase extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function prepareUrlForRequest($uri): string
    {
        return 'api/' . $uri;
    }

}
