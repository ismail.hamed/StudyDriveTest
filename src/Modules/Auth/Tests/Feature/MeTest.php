<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Student\Models\Student;
use Modules\Student\Transformers\StudentResource;
use Symfony\Component\HttpFoundation\Response;


class MeTest extends ApiTestCase
{

    private function request_data()
    {
        return [
            'email' => $this->faker()->email,
            'password' => $this->faker()->password,
        ];
    }

    public function test_me()
    {
        $student = Student::factory()->create();
        $response = $this->actingAs($student)->getJson('auth/me');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonFragment(
            [
                'id' => $student->id,
                'name' => $student->name,
                'email' => $student->email,
                'created_at' => $student->created_at,
                'updated_at' => $student->updated_at,
            ]
        );
    }

    private function dataResponse($user)
    {
        $userInformation = new StudentResource($user);
        return json_decode($userInformation->toJson(), true);
    }

    public function test_me_if_not_registered()
    {
        $response = $this->getJson('auth/me', $this->request_data());
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => "Unauthorized", 'status_code' => Response::HTTP_UNAUTHORIZED]);
    }

}
