<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Student\Models\Student;
use Modules\Student\Transformers\StudentResource;
use Symfony\Component\HttpFoundation\Response;


class LoginTest extends ApiTestCase
{
    public function test_email_is_required()
    {
        $response = $this->postJson('auth/login', array_merge($this->request_data(), ['email' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The email field is required.',
                'errors' => [
                    "email" => ['The email field is required.'],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    private function request_data()
    {
        return [
            'email' => $this->faker()->email,
            'password' => $this->faker()->password,
        ];
    }

    public function test_password_is_required()
    {
        $response = $this->postJson('auth/login', array_merge($this->request_data(), ['password' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The password field is required.',
                'errors' => [
                    "password" => ['The password field is required.'],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    public function test_login()
    {
        $userData = $this->request_data();
        $user = Student::factory()->create($userData);
        $response = $this->postJson('auth/login', $userData);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure(
            [
                'data' => [
                    'access_token',
                    'token_type',
                    'expires_in',
                    'me',
                ]
            ]
        );
        $dataResponse = json_decode($response->getContent(), true);
        $this->assertEqualsCanonicalizing($dataResponse['data']['me'], $this->dataResponse($user));
    }

    private function dataResponse($user)
    {
        $userInformation = new StudentResource($user);
        return json_decode($userInformation->toJson(), true);
    }

    public function test_login_user_if_not_registered()
    {
        $response = $this->postJson('auth/login', $this->request_data());
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => "username or password isn't correct", 'status_code' => Response::HTTP_UNAUTHORIZED]);
    }

}
