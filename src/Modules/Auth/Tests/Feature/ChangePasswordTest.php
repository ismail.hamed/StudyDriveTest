<?php

namespace Modules\Auth\Tests\Feature;

use Illuminate\Support\Facades\Hash;
use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Student\Models\Student;
use Symfony\Component\HttpFoundation\Response;

class ChangePasswordTest extends ApiTestCase
{
    public function test_change_password_by_user_not_authorized()
    {
        $response = $this->postJson('auth/change-password', $this->request_data());
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    private function request_data()
    {
        return
            [
                'current_password' => 'secret',
                'new_password' => 'new_password',
                'new_password_confirmation' => 'new_password'
            ];
    }

    public function test_old_password_is_required()
    {
        $student = Student::factory()->create();
        $response = $this->actingAs($student)->postJson('auth/change-password', array_merge($this->request_data(), ['current_password' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The current password field is required.',
                'errors' => [
                    "current_password" => ['The current password field is required.'],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    public function test_new_password_is_required()
    {
        $student = Student::factory()->create();

        $response = $this->actingAs($student)->postJson('auth/change-password', array_merge($this->request_data(), ['new_password' => '']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The new password field is required.',
                'errors' => [
                    "new_password" => ['The new password field is required.'],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    public function test_not_valid_passowrd()
    {
        $student = Student::factory()->create();

        $response = $this->actingAs($student)->postJson('auth/change-password', array_merge($this->request_data(), ['current_password' => '123456']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'given password does not match',
                'errors' => [
                    "current_password" => ['given password does not match'],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    public function test_password_less_than_six_character()
    {
        $student = Student::factory()->create();
        $response = $this->actingAs($student)->postJson('auth/change-password', array_merge($this->request_data(), ['new_password' => '12345']));
        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson([
                'message' => 'The new password must be at least 6 characters. (and 1 more error)',
                'errors' => [
                    "new_password" => [
                        "The new password must be at least 6 characters.",
                        "The new password confirmation does not match."],
                ],
                "status_code" => Response::HTTP_UNPROCESSABLE_ENTITY
            ]);
    }

    public function test_change_passowrd()
    {
        $student = Student::factory()->create();
        $response = $this->actingAs($student)->postJson('auth/change-password', $this->request_data());
        $response->assertStatus(Response::HTTP_OK)
            ->assertJson(
                ['message' => "User's password updated successfully.", 'status_code' => 200]);
        $this->assertTrue(Hash::check('new_password', $student->password));
    }

}
