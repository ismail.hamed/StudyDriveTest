<?php

namespace Modules\Auth\Tests\Feature;

use Modules\Core\Tests\Feature\ApiTestCase;
use Modules\Student\Models\Student;
use Symfony\Component\HttpFoundation\Response;

class LogoutTest extends ApiTestCase
{

    public function test_logout_user()
    {
        $student = Student::factory()->create();
        $student->createToken('auth_token')->plainTextToken;
        $response = $this->actingAs($student)
            ->getJson('auth/logout');
        $response
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['message' => 'Successfully logged out']);
    }

    public function test_logout_if_user_not_authenticated()
    {
        $response = $this->getJson('auth/logout');
        $response
            ->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJson(['message' => 'Unauthorized']);
    }

}
