<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


use Modules\Auth\Http\Controllers\API\AuthController;

Route::prefix('auth')->group(function () {
    Route::controller(AuthController::class)->group(function () {
        Route::middleware(['auth:sanctum'])->group(function () {
            Route::get('logout', 'logout');
            Route::get('me', 'me');
            Route::post('change-password', 'changePassword');
        });
        Route::get('refresh', 'refresh');
        Route::post('login', 'login');
        Route::post('register', 'register');
    });

});
