<?php

namespace Modules\Auth\Http\Controllers\API;

use Modules\Auth\Http\Requests\ChangePasswordRequest;
use Modules\Auth\Http\Requests\LoginRequest;
use Modules\Auth\Http\Requests\RegisterRequest;
use Modules\Auth\Services\AuthService;
use Modules\Core\Http\Controllers\API\ApiBaseController;
use Modules\Student\Transformers\StudentResource;

/**
 * @group Auth management
 *  APIs for login, register and all about auth
 */
class AuthController extends ApiBaseController
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Login
     *
     * This endpoint lets you to login with specific student
     * @unauthenticated
     * @responseFile Modules/Auth/Storage/responses/login.json
     * @param LoginRequest $request
     * @return mixed
     */

    public function login(LoginRequest $request)
    {
        $credentials = request(['email', 'password']);
        $result = $this->authService->login($credentials);
        if ($result['result']) {
            $data = &$result['data'];
            $data['me'] = new StudentResource($data['me']);
        }
        return $this->apiResponse($result);
    }

    /**
     * Register
     *
     * This endpoint lets student to register new account
     * @unauthenticated
     * @responseFile Modules/Auth/Storage/responses/register.json
     * @param RegisterRequest $request
     * @return mixed
     */
    public function register(RegisterRequest $request)
    {
        $result = $this->authService->register($request->validated());
        return $this->apiResponse($result);
    }

    /**
     * Me
     *
     * This endpoint lets student to get his information
     * @authenticated
     * @responseFile Modules/Auth/Storage/responses/me.json
     * @return mixed
     */
    public function me()
    {
        $result = $this->authService->me();
        if ($result['result']) {
            $result['data'] = new StudentResource($result['data']);
        }
        return $this->apiResponse($result);
    }

    /**
     * Logout
     *
     * This endpoint lets student to logout
     * @authenticated
     * @responseFile Modules/Auth/Storage/responses/logout.json
     * @return mixed
     */

    public function logout()
    {
        $result = $this->authService->logout();
        return $this->apiResponse($result);
    }

    /**
     * Change password
     *
     * This endpoint lets student to change password
     * @authenticated
     * @param ChangePasswordRequest $request
     * @responseFile Modules/Auth/Storage/responses/change-password.json
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $result = $this->authService->changePassword($request->validated());
        return $this->apiResponse($result);
    }

}
