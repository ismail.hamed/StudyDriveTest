<?php


namespace Modules\Auth\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;


class RegisterRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'unique:students'],
            'password' => ['required', 'min:6', 'confirmed'],

        ];
    }
}
