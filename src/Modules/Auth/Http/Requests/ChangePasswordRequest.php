<?php


namespace Modules\Auth\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Modules\Auth\Rules\ValidCurrentUserPassword;


class ChangePasswordRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'current_password' => ['required', 'min:6', new ValidCurrentUserPassword],
            'new_password' => ['required', 'min:6', 'confirmed'],
        ];
    }
}
